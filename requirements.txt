fastapi==0.75.1
pytest==7.1.1
python-dotenv==0.19.0
requests==2.26.0
uvicorn==0.13.4
json