import json
import requests
import random
import time

f = open('example.json')
data = json.load(f)

playlist = {}
artists = []
notes = []

for artist in data :
    artists.append(artist["artiste"])
    notes.append(artist["note"])

total = sum(notes)

for i in range(len(notes)):
    notes[i] = notes[i]/total

cumul = [notes[0]]
for i in range(1, len(notes)):
    cumul.append(notes[i] + cumul[-1])

number = 1
while (number < 21):
    alea = random.random()
    i = 0
    while (cumul[i] < alea):
        i = i + 1
        time.sleep(2)
        current_artist = artists[i].replace(" ", "%20")
        dict_song = requests.get("http://127.0.0.1:8000/random/" + current_artist)
        playlist[number] = dict_song.json()
        number = number + 1

print(playlist)