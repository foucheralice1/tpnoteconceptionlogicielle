import unittest
import lyricsOVH


class TestOVh(unittest.TestCase):

    def test_lyrics(self):
        result = lyricsOVH.lyrics("Daft%20Punk","Alive")
        self.assertEqual(result, "(Instrumental)")

if __name__ == '__main__':
    unittest.main()