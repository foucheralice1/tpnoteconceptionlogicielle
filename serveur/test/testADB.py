import unittest
import theaudiodb

class TestADB(unittest.TestCase):

    def test_artist_id(self):
        result = theaudiodb.get_artist_id_from_name("Daft%20Punk")
        self.assertEqual(result, "111492")

if __name__ == '__main__':
    unittest.main()