import requests
import random

def health_check():
    result = requests.get("https://www.theaudiodb.com/api/v1/json/2/search.php?s=Taylor%20Swift")
    return result

def get_artist_id_from_name(artist_name):
    result = requests.get("https://www.theaudiodb.com/api/v1/json/2/search.php?s=" + artist_name)
    result = result.json()["artists"][0]["idArtist"]
    return result


def random_album_from_artist(artist_id):
    result = requests.get("https://theaudiodb.com/api/v1/json/2/album.php?i=" + artist_id)
    rand = len(result.json()["album"])
    result = result.json()["album"][random.randint(0, rand-1)]["idAlbum"]
    return result


def random_song_from_album(album_id):
    result = requests.get("https://theaudiodb.com/api/v1/json/2/track.php?m=" + album_id)
    rand = len(result.json()["track"])
    rand = random.randint(0, rand-1)
    track = result.json()["track"][rand]["strTrack"]
    music_vid = result.json()["track"][rand]["strMusicVid"]
    result = [track, music_vid]
    return result


def random_song_from_artist(artist_name):
    artist_id = get_artist_id_from_name(artist_name)
    album_id = random_album_from_artist(artist_id)

    title = random_song_from_album(album_id)[0]
    youtube = random_song_from_album(album_id)[1]
    return [title, youtube]
