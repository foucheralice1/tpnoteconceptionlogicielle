import serveur.theaudiodb as tadb
import serveur.lyricsOVH as lovh
from fastapi import FastAPI


app = FastAPI()


@app.get("/")
async def root():
    tadb_health = str(tadb.health_check())
    lovh_health = str(lovh.health_check())
    return {"etat_de_sante_de_TheAudioDB": tadb_health,
        "etat_de_sante_de_LyricsOVH": lovh_health}

@app.get("/random/{artist_name}")
async def random_song_from_artist(artist_name):
    result_tadb = tadb.random_song_from_artist(artist_name)
    title = result_tadb[0]
    youtube = result_tadb[1]
    lyrics = lovh.lyrics(artist_name, title)

    return {"artiste": artist_name,
        "titre": title,
        "lien_youtube_suggere": youtube,
        "paroles": lyrics
        }
