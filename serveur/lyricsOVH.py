import requests

def health_check():
    result = requests.get("https://api.lyrics.ovh/v1/Taylor%20Swift/22")
    return result

def lyrics(artist_name, title):
    result = requests.get("https://api.lyrics.ovh/v1/" + artist_name + "/" + title)
    return result.json()["lyrics"]
