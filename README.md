# Tp conception log

Cette application met à disposition les données de theaudiodb et lyricsOVH afin de constituer une playlist de 20 chansons avec des artistes sélectionnés.

## Quickstart

### Serveur

En utilisant git, via le terminal :

```bash
git clone https://gitlab.com/foucheralice1/tpnoteconceptionlog
```

Utilisez le gestionnaire de paquet [pip](https://pip.pypa.io/en/stable/) afin d'installer les dépendances
(linux/macOS)

```bash
pip install -r requirements.txt
```

avec le cmd de windows :

```bash
python -m pip install -r requirements.txt
```

Deployez le serveur avec:

```bash
cd Server
python server.py
```

### Client

Déposer le fichier JSON contenant le nom des artistes dans le dossier client. Exemple du format à respecter :
[
{
"artiste": "daft punk",
"note": 18
}
]

Dans le fichier client ligne 6, remplacer "example" par le nom de votre fichier.

## Usage

Voici l'adresse par défaut sur laquelle l'API créée est consommable : http://127.0.0.1:8000/

les endpoints suivants permettent d'avoir accès :

- à une musique au hasard de l'artiste
  /random/{artist_name]

- aux états de santé des sorties
  /

## Test

Il est possible de tester le bon fonctionnement de notre application :
En ouvrant un terminal à la racine du projet:

```bash
cd serveur
python -m unittest test/testOVH.py
python -m unittest test/testADB.py
```

## Schema utilisé dans l'application pour utiler les API lyricsOVH et theaudiodb.

```mermaid
graph TB
    artist_name-->lyrics
    title-->lyrics
    subgraph lyricsOVH
    lyrics
    end
    subgraph theaudiodb
    artist_name-->artist_id;
    artist_id-->|random album|album_id;
    album_id-->|random title|title;
    album_id-->lien_youtube;
    end
  
```


## Contributing

Si vous voulez contribuer à ce projet, n'hésitez pas à ouvrir une issue.

## License

[GPL-3.0](https://www.gnu.org/licenses/gpl-3.0.en.html)
